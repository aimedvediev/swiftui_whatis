//
//  DetailView.swift
//  practiceSwiftUI
//
//  Created by Anton Medvediev on 03/04/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import SwiftUI
import WebKit

struct DetailView: View {
    
    let url: String?
    
    var body: some View {
        WebView(urlString: url)
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(url: "gttps://www.google.com")
    }
}


